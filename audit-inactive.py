# import json

# # Create a dictionary to store the collected data
# collected_data = {}

# # Open the JSON file
# with open('./export-1697874273.json', 'r') as file:
#     for line in file:
#         try:
#             entry = json.loads(line)

#             # Extract the relevant fields
#             actor = entry.get('actor')
#             external_identity_username = entry.get('external_identity_username')
#             user = entry.get('user') 

#             if actor:
#                 if actor not in collected_data:
#                     collected_data[actor] = {'external_identity_username': [], 'user': []}
#                 if external_identity_username:
#                     collected_data[actor]['external_identity_username'].append(external_identity_username)
#                 if user:
#                     collected_data[actor]['user'].append(user)
#         except json.JSONDecodeError:
#             # Handle invalid JSON lines here
#             print(f"Invalid JSON line: {line}")

# # Print the collected data
# for actor, values in collected_data.items():
#     print(f'Actor: {actor}')
#     print(f'External Identity Usernames: {values["external_identity_username"]}')
#     print(f'Users: {values["user"]}')
#     print()

# # You can also save the collected data to a new JSON file if needed
# with open('collected_data.json', 'w') as output_file:
#     json.dump(collected_data, output_file, indent=4)

import json
from datetime import datetime
import sys

def ts2rts(ts):
    # Convert the Unix timestamp to a datetime object
    timestamp = ts / 1000  # Divide by 1000 to convert from milliseconds to seconds
    date_time = datetime.fromtimestamp(timestamp)

    # Format the datetime object as a string in a readable format
    formatted_date_time = date_time.strftime('%Y-%m-%d %H:%M:%S')
    return formatted_date_time

collected_data = {}
with open('./log.json', 'r') as file:
    for line in file:
        try:
            entry = json.loads(line)
            actor_id = entry.get('external_identity_username')
            action = entry.get('action')
            ts = entry.get('@timestamp')

            if actor_id and ts and action:
                if actor_id not in collected_data:
                    collected_data[actor_id] = [action + " in " + ts2rts(ts)]
                else:
                    collected_data[actor_id].append(action + " in " + ts2rts(ts))
                
        except json.JSONDecodeError:
            # Handle invalid JSON lines here
            print(f"Invalid JSON line: {line}")  

for key, value in collected_data.items():
    print(len(value), key, value)

